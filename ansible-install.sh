#1/bin/bash
distro=$(cat /etc/*-release | grep "ID=ubuntu|ID=centos|ID=debian" | awk '{ print $2 }' FS="=")
distro_version=$(cat /etc/*-release | grep "ID=" | awk '{ print $2 }' FS="=")
case $dist_version in
	ubuntu)
		apt install software-properties-common -y
		apt-add-repository --yes --update ppa:ansible/ansible
		apt install ansible -y
		;;
	centos)
		yum install ansible -y
		;;	
	debian)
		echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
		apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
		apt update
		apt install ansible
		;;

esac

